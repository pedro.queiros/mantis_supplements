# mantis_supplements

Contents description:
- alignments: contains Diamond-generated alignments of Prokka's gene calling and the reference proteome
- code: contains code for the manuscript's benchmark, as well as sample generation code
- figures: figures from the manuscript
- reference_data: reference proteomes,genomes, and annotations.
- results: results from Mantis and other tools
